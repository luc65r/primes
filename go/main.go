package main

import (
	"os"
	"fmt"
	"time"
	"strconv"
)

func main() {
	n, _ := strconv.ParseUint(os.Args[1], 10, 64)

	f, err := os.Create("primes.txt")
	if err != nil {
		panic(err)
	}

	defer f.Close()

	alloc := n / 8 + 1
	primes := make([]byte, alloc)

	fmt.Printf("Initializing %v bytes of memory...\n", alloc)
	start := time.Now()
	initArray(alloc, primes)
	fmt.Printf("Done in %v.\n\n", time.Since(start))

	c := make(chan uint64)
	go elimNp(n, primes, c)

	for i := range c {
		f.WriteString(fmt.Sprintln(i))
	}

	f.Sync()
}

func initArray(bytes uint64, primes []byte) {
	primes[0] = 0b00110101
	for i := uint64(1); i < bytes; i++ {
		primes[i] = 0b01010101
	}
}

func elimNp(n uint64, primes []byte, c chan uint64) {
	defer close(c)

	for i := uint64(3); i <= n; i += 2 {
		if (primes[i / 8] >> (7 - byte(i % 8))) & 1 == 0 {
			continue
		}
		c <- i
		for j := uint64(3); ; j += 2 {
			m := i * j
			if m > n { break }
			primes[m / 8] &= 0xFF ^ (1 << (7 - byte(m % 8)))
		}
	}
}

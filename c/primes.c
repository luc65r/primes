#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <string.h>

#define u64 uint_fast64_t

int main(int argc, char **argv) {
    if (argc != 2)
        return fputs("Please supply one argument\n", stderr), 1;

    u64 n = strtoumax(argv[1], NULL, 10);
    if (!n) return fputs("Please supply a valid number > 0\n", stderr), 1;
    if (n == 1) return 0; /* 1 isn't prime, so we exit right away */

    /* Allocate the array */
    uint8_t *primes = malloc(n / 8 + 1);
    /* Set 0, 1 and all evens to 0; 2, 3 and all odds to 1 */
    *primes = 0b00110101;
    memset(primes + 1, 0b01010101, n / 8);

    /* Eliminate all non primes in the array by setting them to 0
     * using the sieve of Eratosthenes */
    for (u64 i = 3; i <= sqrt(n); i += 2) {
        if (!(primes[i / 8] >> (7 - i % 8) & 1)) continue;
        for (u64 j = 3, m; m = i * j, m <= n; j += 2)
            primes[m / 8] &= ~(1 << (7 - m % 8));
    }

    /* Allocate the buffer for the string repesenting the prime */
    char *num = malloc((u64)log10(n) + 1);

    puts("2"); /* n >= 2, we can print it now */
    for (u64 i = 3; i <= n; i += 2)
        if (primes[i / 8] >> (7 - i % 8) & 1) {
            int m = 0;
            /* Decompose the number into ascii digits (reversed) */
            for (u64 j = i; j > 0; j /= 10)
                num[m++] = j % 10 + '0';

            /* Print the buffer contents */
            while (m > 0) putchar(num[--m]);
            putchar('\n');
        }

    free(num); free(primes);
    return 0;
}
